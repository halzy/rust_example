use rand::{self, rngs::ThreadRng, Rng};
use actix::prelude::*;

pub struct DataStuff {
    rng: ThreadRng
}

impl Actor for DataStuff {
    type Context = SyncContext<Self>;
}

enum Msg {
    Hello,
    Goodbye
}

impl actix::Message for Msg {
    type Result = Result<u32, u32>;
}

impl Handler<Msg> for DataStuff {
    type Result = Result<u32, u32>;

    fn handle(&mut self, msg: Msg, _ctx: &mut SyncContext<Self>) -> Self::Result {
        match msg {
            Msg::Hello => {
                print!("Hello!");
                Ok(self.rng.gen::<u32>())
            }
            Msg::Goodbye => {
                print!("Goodbye!");
                Err(self.rng.gen::<u32>())
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use actix::prelude::*;
    use tokio;
    use futures::{future, Future};

    #[test]
    fn it_works() {
        System::run(move || {
            let data_stuff = SyncArbiter::start(1, move || crate::DataStuff {
                rng: rand::thread_rng(),
            });

            actix::Arbiter::spawn(data_stuff.send(crate::Msg::Hello).then(move |result| {
                if let Ok(Ok(id)) = result {
                    print!("AOEU");
                } else {
                    print!("UEOA");
                }
                System::current().stop(); // Needs to be in last test
                future::result(Ok(()))
            }));
        });
    }
}
